const express = require('express');
const app = express();
const path = require('path');
const hbs = require('express-handlebars');
const os = require('os');
const fs = require('fs');

console.log(os.freemem() / 100000000);

let routes = require('./routes/index.route');

//todo testing file
// fs.readFile('data.txt', function (err, data) {
//   if (err)
//     throw err;
//   if (data)
//     console.log(data.toString('utf8'));
// });

//set the template engine
//extname the extension of our tpl files (ex: view.hbs)
let options = {
  extname: 'hbs',
  defaultLayout: 'layout',
  layoutsDir: __dirname + '/views/layouts'
};
app.engine('hbs', hbs(options));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use('/', routes);

//Make public dir accessible by our app
app.use(express.static(__dirname + '/public'));


app.listen(8080, function(){
  console.log('Listening on port:8080');
});

module.exports = app;