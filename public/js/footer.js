$(function() {
  // Handler for .ready() called.

  var screen_height = $(window).height();
  var html_height = $('html').height();

  if(html_height < screen_height){
    $('footer').addClass('sticky');
    $('html').css('height', screen_height);
  }
});