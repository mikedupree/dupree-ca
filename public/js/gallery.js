//body > div.container.no-header > div > div:nth-child(7) > a > img
$(function() {

  let imgs = document.images,
    len = imgs.length,
    counter = 0;

  if(len > 0) {
    console.log(len, 'len');
    console.log(imgs, 'imgs');
    [].forEach.call(imgs, function (img) {
      img.addEventListener('load', incrementCounter, false);
    });
  } else {
    $('body').removeClass('loading');
  }

  function incrementCounter() {
    counter++;
    if ( counter === len ) {
      console.log('remove loading class');
      $('body').removeClass('loading');
    }
  }

  $(window).on('load', function(){
    console.log('all is loaded!');
    $('body').removeClass('loading');
  });
});