$(function() {
  // Handler for .ready() called.

  var url = window.location.toString();
  var protocol = window.location.protocol;
  var host = window.location.host;
  var path = window.location.pathname;

  var i = 0;
  var txt = ' sudo apt update';
  var speed = 150;
  typeWriter();

  function typeWriter() {
    if (i < txt.length) {
      document.getElementById("typer").innerHTML += txt.charAt(i);
      i++;
      setTimeout(typeWriter, speed);
    } else {
      i = 0;
      document.getElementById("typer").innerHTML = "";
      setTimeout(typeWriter, speed);
    }
  }


  if(path === "/"){
    $('.nav-item .active').removeClass('active');
    $('.nav-item .home').addClass('active');
  } else {
    $('.nav-item .active').removeClass('active');
    $('.nav-item .' + path.replace("/","")).addClass('active');
  }

});