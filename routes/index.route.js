const express = require('express');
const router = express.Router();
const fs = require('fs');
const request = require('request');

const personalInfo = {
  tab_title: "contact | dupree.ca",
  title1: "Mike Dupree ",
  title2: " Developer",
  headline: "Hello World",
  host: 'http://localhost:8080',
};

let populateImages = function(callback){
  fs.readdir('./public/images/gallery/', (err, files) => {
    if(files.length > 0) {
      personalInfo.gallery = [];
      let i = 0;
      files.forEach(file => {
        personalInfo.gallery[i++] = {
          path: "/images/gallery/" + file,
          name: file,
        };
      });
    }
    callback();
  });
};

//gtilab access token: NmVNsohryy8HP61nrayK

// Configure the request to grab Github data
let options = {
  url: 'https://gitlab.com/api/v4/users/mikedupree/projects',
  method: 'GET',
  headers: {
    'User-Agent': 'dupree.ca',
  },
  qs: {}
};



/* GET home page. */
router.get('/', function(req, res){
  res.render('index', personalInfo);
});

/* GET projects page. */
router.get('/projects', function(req, res){
  let gitlab_projects_temp;
  let gitlab_projects = [];

// Start the request to grab Github data
  request(options, function (error, response, body) {
    if (!error && response.statusCode == 200) {gitlab_projects_temp = JSON.parse(body);}
    for(let i = 0; i < gitlab_projects_temp.length; i++){

      if(i==0)
        console.log(new Date(gitlab_projects_temp[i].last_activity_at).toDateString());

      gitlab_projects[i] = {};
      gitlab_projects[i].description = gitlab_projects_temp[i].description;
      gitlab_projects[i].name = gitlab_projects_temp[i].name;
      gitlab_projects[i].url = gitlab_projects_temp[i].web_url;
      gitlab_projects[i].avatar = (gitlab_projects_temp[i].avatar_url == null) ? 'http://placehold.it/500x325' : gitlab_projects_temp[i].avatar_url;
      gitlab_projects[i].lastActive = new Date(gitlab_projects_temp[i].last_activity_at).toDateString();
    }
    personalInfo.projects = gitlab_projects;
    res.render('projects', personalInfo);

  });

});

/* GET gallery page. */
router.get('/gallery', function(req, res){
  populateImages(() =>{
    res.render('gallery', personalInfo)});
});

/* GET contact page. */
router.get('/contact', function(req, res){
  res.render('contact', personalInfo);
});

/* GET about page. */
router.get('/about', function(req, res){
  res.render('about', personalInfo);
});

module.exports = router;